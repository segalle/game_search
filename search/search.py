from collections import Counter

from django.core.cache import caches

from indexer.index import get_clean_terms


SCORE_BOOST = {
    'accuracy': 0.7,
    'rarity': 0.3,
}

cache = caches['default']


class SearchEngine:

    """
    I Used Class object in order to be able to inherit in the future and change the grade system (for example).

    """

    def __init__(self):
        self.rare_index = {}
        self.search_terms = []
        self.scores = {}
        self.matches = []
        self.commons = []
        self.res = {}

        self.key_index = cache.get('key_index', {})

    def search(self, q):
        """
        Search the in-memory index for games
        :param q: search terms
        :return: sorted list (by score) of all matching games
        """
        self.search_terms = get_clean_terms(q)
        self.find_matches()
        if not self.commons:
            return []

        self.grade()

        return sorted(self.res.values(), key=lambda x: x['score'], reverse=True)

    def find_matches(self):

        """
        Search for matching entities for every search term
        :return: Return only results that are matching all the search terms
        """

        all_matches = []

        for term in self.search_terms:
            matches = self.key_index.get(term, set())
            self.rare_index[term] = len(matches)
            self.matches += matches

            all_matches.append(matches)

        self.commons = all_matches[0]
        for m in all_matches:
            self.commons.intersection_update(m)

        self.res = {match: cache.get('objects_index', {})[match] for match in self.commons}

    def grade(self):
        """
        Add score to all search results for sorting purposes

        Score is calculated by 2 parameters:
        accuracy - how close is the search term to the game name
        rarity - Rare terms results getting higher grade
        """
        accuracy = {k: v / cache.get('titles_len_index', {}).get(k) for k, v in Counter(self.matches).items()}

        for term in self.search_terms:
            term_factor = 1 - (self.rare_index[term] / len(self.matches))
            for game in self.key_index.get(term, []):
                if game in self.commons:
                    self.res[game]['score'] = term_factor * SCORE_BOOST['rarity'] + accuracy[game] * SCORE_BOOST['accuracy']