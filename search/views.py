from datetime import datetime

from django.core.cache import caches
from django.shortcuts import render_to_response
from django.views.generic import View
from rest_framework.response import Response
from rest_framework.views import APIView

from indexer.index import fetch_async
from search.search import SearchEngine


class IndexView(APIView):

    def get(self, request):
        return Response({'index_exist': cache.get('key_index', None) is not None})

    def post(self, request):

        fetch_async()

        return Response({'message': 'Background indexing started'})


cache = caches['default']


class SearchView(APIView):

    def get(self, request):

        q = request.GET.get('q')

        start = datetime.now()
        items = SearchEngine().search(q) if q else []
        search_time = (datetime.now() - start).microseconds / 1000

        res = {
            'items': items,
            'search_time': round(search_time, 0),
            'results': len(items),
        }

        return Response(res)


class WebView(View):

    def get(self, request):

        return render_to_response('index.html')
