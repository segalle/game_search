'use strict';

/**
 * @ngdoc function
 * @name webApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the webApp
 */
angular.module('webApp')
  .controller('MainCtrl', function ($scope, $http, appConfig, $httpParamSerializer) {
    $scope.search_data = {
      q: ''
    };

    $scope.results = [];

    $scope.search = function() {
      var qs = $httpParamSerializer($scope.search_data);

      $http.get(appConfig.url + '/search?' + qs)
        .success(function (data) {
          $scope.results = data;
        });
    };

    function checkIndex () {
        $http.get(appConfig.url + '/index')
            .success(function (data) {
               $scope.index_exist = data.index_exist;
            });
    }

    checkIndex();


    $scope.makeIndex = function () {
        $http.post(appConfig.url + '/index')
            .success(function () {
                $scope.index_exist = true;
            });
    };
  });
