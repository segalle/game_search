import requests
from django.conf import settings

GIANTBOMB_BASE_URL = 'http://www.giantbomb.com/api/'

GAMES_ENDPOINT = 'games'

PLATFORMS = {
    9: 'SNES',
    21: 'NES',
    43: 'N64',
}


def request_giantbomb(endpoint, params):

    '''
    Requests Giantbomb API and return the results
    :param endpoint: Giantbomb api endpoint
    :param params: url params for the GET request
    :return: request data or raises an exception
    '''

    url = GIANTBOMB_BASE_URL + endpoint

    params['api_key'] = settings.GIANTBOMB_API_KEY

    res = requests.get(url, params=params)

    res.raise_for_status()
    res_data = res.json()

    if res_data['status_code'] != 1:
        raise Exception(res_data['error'])

    return res_data


def get_games(offset=0):

    """
    Request games from API
    :param offset: List offset for batches
    :return: list of games
    """

    params = {
        'format': 'json',
        'offset': offset,
        'platforms': ','.join([str(k) for k in PLATFORMS.keys()]),
        'field_list': 'name,deck,site_detail_url,description,image',
        'limit': 100
    }

    return request_giantbomb(GAMES_ENDPOINT, params)['results']
