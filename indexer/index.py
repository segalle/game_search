import logging
import re
import threading

from django.core.cache import caches

from indexer.giantbomb import get_games

STOP_WORDS = ['a', 'an', 'and', 'are', 'as', 'at', 'be', 'by', 'for', 'in', 'it', 'of', 'on', 'the', 'to']


logger = logging.getLogger('django')


def get_clean_terms(q):
    """
    Clean the search terms from any non alphanumeric character
    :param q: search query
    :return: list of clean terms
    """
    clean_term = re.sub(r'\W+', ' ', q)
    return [t.lower() for t in clean_term.split()]


def fetch_games_generator():
    """
    Create bulk requests to the games api
    :return: list of all games entities
    """
    offset = 0
    eof = False

    while not eof:
        logger.debug('Fetching games from GiantBomb - offset {}'.format(offset))

        try:
            games = get_games(offset=offset)

        except Exception as ex:
            logger.error(str(ex))
            yield []

        else:
            yield games

        finally:
            offset += 100
            if len(games) < 100:
                eof = True

    logger.debug('Fetching completed'.format(offset))

cache = caches['default']


def clean_cache():
    cache.delete('key_index')
    cache.delete('objects_index')
    cache.delete('titles_len_index')


def fetch_and_index():

    '''
    Index game entities and puts results in cache
    '''

    clean_cache()

    key_index = {}
    objects_index = {}
    titles_len_index = {}

    current_id = 0

    for batch in fetch_games_generator():
        for game in batch:
            splitted_clean_name = get_clean_terms(game['name'])

            for word in splitted_clean_name:
                # search terms are case insensitive
                word = word.lower()

                if word in STOP_WORDS or len(word) < 2:
                    # Skip "stop words" and short words
                    continue

                if word not in key_index:
                    key_index[word] = set()

                key_index[word].add(current_id)

            titles_len_index[current_id] = len(splitted_clean_name)
            objects_index[current_id] = game

            current_id += 1

        '''
        key_index - a dictionary with words as keys and item ids set as vales
        objects_index - a dictionary of all objects
        titles_len_index - a dict that contains the length of all titles (for scoring purposes)
        '''

        cache.set('key_index', key_index, None)
        cache.set('objects_index', objects_index, None)
        cache.set('titles_len_index', titles_len_index, None)


def fetch_async():
    t = threading.Thread(target=fetch_and_index)
    t.start()
