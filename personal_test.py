from datetime import datetime

import django

from search.search import SearchEngine

django.setup()

from indexer.index import fetch_and_index

fetch_and_index()

start = datetime.now()
res = SearchEngine().search('team super')
search_time = datetime.now() - start

result = {
    'items': res,
    'search_time': search_time.microseconds,
    'results': len(res)
}
print(result)